
CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline
CFLAGS+=-D_BSD_SOURCE

LDLIBS+=-lm

egg_drop: egg_drop.o egg.o

.PHONY: clean debug

clean:
	-rm *.o

debug: CFLAGS+=-g
debug: egg_drop

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: egg_drop
