#include <math.h>
#include <stdio.h>
#include <ctype.h>

#include "egg.h"

void egg_scrambler(int eggnum, size_t floornum, egg **basket);

//Function that uses that quadratic formula to find the optimal number to start dropping the egg at
int optimal_floor_drop(int floornum){

	double a = 0.5;
	double b = 0.5; 
	double c = -floornum;

	double result = (-b+sqrt(pow(b, 2) - 4*a*c))/(2*a);
	return ceil(result);
}

int main(int argc, char *argv[]){

	//Error checking
	if(argc != 3){
		fprintf(stderr, "Invalid input!\n");
		return 0;
	}

	//Grabbing the floornum and error checking
	size_t floornum = strtol(argv[1], NULL, 10);
	if(!floornum || (signed)floornum < 0){
		fprintf(stderr, "Invalid floor value!\n");
		return 1;
	}

	//Grabbing the eggnum and error checking
	size_t eggnum = strtol(argv[2], NULL, 10);
	if(!eggnum || (signed)eggnum < 0){
		fprintf(stderr, "Invalid egg value!\n");
		return 1;
	}


	//Creating space for the array that will hold the eggs
	egg **basket = malloc(sizeof(*basket) * eggnum);
	if(!basket){
		return 1;
	}
	
	for (int i = 0; i < (signed)eggnum; ++i){
		basket[i] = lay_egg();
	}

	egg_scrambler(eggnum, floornum, basket);
}

void egg_scrambler(int eggnum, size_t floornum, egg **basket){

	int egg_tracker = eggnum;	
	int number_of_floors_in_building = floornum;
	int highest_floor = number_of_floors_in_building;

	size_t floor_about_to_drop_egg_from = optimal_floor_drop(floornum);
	size_t step = floor_about_to_drop_egg_from;
	size_t previous = 1;
	int counter = 0, num_of_drops = 0, building_is_shorter = 0;

	//Loop that runs while there is >= 1 egg
	while(1){
		//static size_t floor_about_to_drop_egg_from = 1;
		//If eggnum is greater than 1 then go in and drop the egg, check to see if the egg breaks, if it does continue on and print out that floor, increment and decrement variables,
		//as well as more checking
		if(eggnum > 1){
			egg_drop_from_floor(basket[eggnum-1], floor_about_to_drop_egg_from);
			//If the egg is broken then enter and execute the code
			if(egg_is_broken(basket[eggnum-1])){
				printf("#%ld CRACK\n", floor_about_to_drop_egg_from);
				highest_floor = floor_about_to_drop_egg_from;
				++num_of_drops;
				--eggnum;
				floornum = previous;
				floor_about_to_drop_egg_from = optimal_floor_drop(highest_floor - previous)+previous;
				//Check to see if the floor that is being dropped from is greater than or equal to the current floornum
				if(floor_about_to_drop_egg_from+previous >= floornum){
					floor_about_to_drop_egg_from = optimal_floor_drop(floor_about_to_drop_egg_from - previous);
				}
				step = floor_about_to_drop_egg_from;
				//Counter is used in the case that the first time the code was executed the egg cracked
				if(counter == 0){
					floor_about_to_drop_egg_from = 1;
				}
				//Otherwise just continue on
				else{
					floor_about_to_drop_egg_from = previous+1;
					egg_drop_from_floor(basket[eggnum-1], floor_about_to_drop_egg_from);
					//Make a quick check to see if the egg is broken at this point and if so exit the loop
					if(egg_is_broken(basket[eggnum-1])){
						break;	
					}
					--step;
				}
			}
			//If the egg did not break then print out safe, increment the variables, do more checking
			else{
				printf("#%zd safe\n", floor_about_to_drop_egg_from);
				++num_of_drops;
				--step;
				//If step is ever less than 1 set it equal to 1
				if(step < 1){
					step = 1;
				}
				previous = floor_about_to_drop_egg_from;
				floor_about_to_drop_egg_from+=step;
				//Check to see if the floor that is being dropped from plus the step is greater than or equal to the number_of_floors_in_building
				if((signed)(floor_about_to_drop_egg_from) >= number_of_floors_in_building){
					floor_about_to_drop_egg_from = number_of_floors_in_building;
					++num_of_drops;
					if(building_is_shorter){
						break;
					}
					building_is_shorter = 1;
				}
				++counter;
				egg_drop_from_floor(basket[eggnum-1], previous+1);
				//If the egg is broken then enter and execute the code
				if(egg_is_broken(basket[eggnum-1])){
					//If the previous floor number plus 1 is greater than or equal to the number of floors in the building then excecute code and break out
					if((signed)previous+1 >= number_of_floors_in_building){
						printf("#%ld CRACK\n", previous+1);
						++num_of_drops;
						break;
					}
					break;
				}
			}
		}
		else{
			if(counter == 0){
				floor_about_to_drop_egg_from = 1;
				++counter;
			}
			egg_drop_from_floor(basket[eggnum-1], floor_about_to_drop_egg_from);
			//Checks to see if the egg is broken, and if so prints and incrments/decrements variables
			if(egg_is_broken(basket[eggnum-1])){
				printf("#%zd CRACK\n", floor_about_to_drop_egg_from);
				++num_of_drops;
				--eggnum;
			}
			//If egg is not broken, print and and incrments variables 
			else{
				printf("#%zd safe\n", floor_about_to_drop_egg_from);
				++num_of_drops;
				++floor_about_to_drop_egg_from;
				//Check to see if the floor that is being dropped from plus the step is greater than or equal to the number_of_floors_in_building
				if((signed)(floor_about_to_drop_egg_from) >= number_of_floors_in_building){
					floor_about_to_drop_egg_from = number_of_floors_in_building;
					++num_of_drops;
					if(building_is_shorter){
						break;
					}
					building_is_shorter = 1;
				}
				egg_drop_from_floor(basket[eggnum-1], floor_about_to_drop_egg_from);
				//Checks to see if the egg being dropped will break and if so breaks out of the loop
				if(egg_is_broken(basket[eggnum-1])){
					break;
				}
			}
		}
	}
	//If building_is_shorter equals 1 then print other wise print out the other statement
	if(building_is_shorter == 1){
		printf("Egg can safely be dropped from maximum provided heigth\n");
	}
	else{
		printf("%zd is the maximum safe floor, found after %d drops\n", floor_about_to_drop_egg_from-1, num_of_drops);
	}
	//Loop that will go through the basket at position j and call the cook_egg function
	for(int j = egg_tracker-1; j >= 0; --j){
		cook_egg(basket[j]);
	}		
	//Free the basket that was created to hold the eggs
	free(basket);
}



